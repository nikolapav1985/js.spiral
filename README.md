Description
-----------

Javascript and HTML5 graphics example. Make a circle and move along spiral trajectory.

Test environment
----------------

os lubuntu 16.04 node version 11.6.0 npm version 6.5.0

Run example
-----------

- type node index.js (launch application on port 8080 http)
- go to http://localhost:8080

Install dependencies
--------------------

- type npm install
