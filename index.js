var express=require("express");
var app=express();
var port=8080;

app.set("view engine","pug");
app.use(express.static('public'))

app.get("/", function(req,res){
    res.render("index",{});
});

app.listen(port,function(){
    console.log("app on port "+port);
});
