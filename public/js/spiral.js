function Circle(context,a,b){ // make a circle
    var context=context;
    var a=a;
    var b=b;
    var i=1;
    var centerx=context.canvas.width/2;
    var centery=context.canvas.height/2;
    var max=720;
    this.moveSpiral = function(){ // move in spiral path
        var angle;
        var x;
        var y;
        context.clearRect(0,0,context.canvas.width,context.canvas.height);
        angle=0.1*i;
        i++;
        i=i%max;
        x = centerx + (a + b * angle) * Math.cos(angle);
        y = centery + (a + b * angle) * Math.sin(angle);
        context.beginPath();
        context.arc(x,y,10,0,Math.PI*2);
        context.fillStyle="#0095DD";
        context.fill();
        context.closePath();
    }
}
